import { v4 as uuidv4 } from 'uuid';
import { v1 as uuidv1 } from 'uuid';

export default class UUIDGenerator {
    generate_v1(callback) {
        let id = uuidv1();
        callback(id);
    }
    generate_v4(callback) {
        let id = uuidv4();
        callback(id);
    }
}