// import './style.css';
var uId = new UUIDGenerator();
var $ = document.querySelector.bind(document);

$("#v1").addEventListener("click", v1);
$("#v4").addEventListener("click", v4);

function v1() {
    uId.generate_v1(function(id) {
        $("#result1").innerHTML = id;
    });
}

function v4() {
    uId.generate_v4(function(id) {
        $("#result4").innerHTML = id;
    });
}

v1();
v4();