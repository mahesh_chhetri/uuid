var $$ = document.querySelectorAll.bind(document);

function copyClipboard(element, tooltipElement) {
    var elm = document.getElementById(element);
    // for Internet Explorer

    if (document.body.createTextRange) {
        var range = document.body.createTextRange();
        range.moveToElementText(elm);
        range.select();
        document.execCommand("Copy");
        tooltipElement.innerHTML = "Copied: UUID to clipboard";
    } else if (window.getSelection) {
        // other browsers
        var selection = window.getSelection();
        var range = document.createRange();
        range.selectNodeContents(elm);
        selection.removeAllRanges();
        selection.addRange(range);
        document.execCommand("Copy");
        tooltipElement.innerHTML = "Copied: UUID to clipboard";
    }
}

// Adding event
// copy event v1
document.getElementById("copy1").addEventListener('click', function(e) {
    e.preventDefault();
    var tooltipElement = e.currentTarget.querySelector('.tooltiptext');
    copyClipboard("result1", tooltipElement);
});
// copy event v4
document.getElementById("copy4").addEventListener('click', function(e) {
    e.preventDefault();
    var tooltipElement = e.currentTarget.querySelector('.tooltiptext');
    copyClipboard("result4", tooltipElement);
});



function outFunc() {
    var tooltip = document.getElementById("myTooltip");
    tooltip.innerHTML = "Copy to clipboard";
}

//  change text
$$("._tooltip").forEach(function(el) {
    el.addEventListener("mouseout", function() {
        el.querySelector('.tooltiptext').innerHTML = "Copy to clipboard";
    })
})