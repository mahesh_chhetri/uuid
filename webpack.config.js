// Webpack uses this to work with directories
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

// This is the main configuration object.
// Here you write different options and tell Webpack what to do
module.exports = {

    // Path to your entry point. From this file Webpack will begin his work
    entry: {
        UUIDGenerator: './src/index.js',
        demo: './src/demo.js',
        copyJs: './src/copy.js'
    },
    mode: 'production',
    module: {
        rules: [{
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.html$/i,
                loader: 'html-loader',
            },
            {
                test: /\.(png|jpe?g|gif)$/i,
                use: [{
                    loader: 'file-loader',
                }, ],
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            }
        ]
    },


    // Path and filename of your result bundle.
    // Webpack will bundle all JavaScript into this file
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist'),
        // libraryTarget: 'umd',
        library: 'UUIDGenerator',
        libraryExport: "default",
        libraryTarget: 'umd'
    },

    // Default mode for Webpack is production.
    // Depending on mode Webpack will apply different things
    // on final bundle. For now we don't need production's JavaScript 
    // minifying and other thing so let's set mode to development
    mode: 'development',
    plugins: [new HtmlWebpackPlugin({
        template: './index.html'
    })]
};